package datastructures;

/**
 * Created by david on 9/16/17.
 */
public enum ArtifactType {
    REQUIREMENT, TESTCASE, SOURCECODE, UML, THREATMODEL
}
