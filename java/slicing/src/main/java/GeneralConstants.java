/**
 * Created by david on 8/28/17.
 */
public class GeneralConstants {
    public static final String METRICS_FILE_EXT = ".csv";
    public static final String METRICS_FILE_PREFIX = "metrics_";
    public static final String JAVA = "java";
    public static final String CS = "c#";
    public static final String JAVA_EXT = ".java";
    public static final String CS_EXT = ".cs";
    public static final String C = ".c";

    public GeneralConstants() {

    }

    public void finalize() throws Throwable {

    }
}
