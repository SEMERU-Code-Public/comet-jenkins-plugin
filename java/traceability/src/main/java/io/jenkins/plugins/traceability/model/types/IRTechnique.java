package io.jenkins.plugins.traceability.model.types;

public enum IRTechnique {
    VSM, LSI, JS, LDA, NMF, VSM_LDA, JS_LDA, VSM_NMF, JS_NMF, VSM_JS
}
